﻿import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import axios from 'axios'
import VueAxios from 'vue-axios'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.config.productionTip = false;
Vue.use(VueAxios, axios);
axios.defaults.withCredentials = true;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");

router.beforeEach((to, from, next) => {
    if (to.matched[0].meta.requiresAuth) {
        var api = 'https://vue-course-api.hexschool.io/api/user/check';
        axios.post(api).then((response) => {
            if (response.data.success) {
                console.log(response);
                next();
            }
            else {
                console.log(response);
                next({
                    path: '/Login',
                });
            }
            console.log(response);

        })
    } else {
        next() 
        console.log(to);
    }
})