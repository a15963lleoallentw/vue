import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../components/Login.vue";
import Dashboard from "../components/Dashboard.vue";
import Products from "../components/Pages/Products.vue";

Vue.use(VueRouter);

const routes = [
    {
        path: '*',
        redirect: 'Login'
    },
    {
        path: "/Admin",
        name: "Dashboard",
        component: Dashboard,
        children: [
            {
                path: "Products",
                name: "Products",
                component: Products,
                meta: { requiresAuth: true },
            }
        ]
    },
    {
        path: "/Login",
        name: "Login",
        component: Login
    },
    {
        path: "/about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/About.vue")
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

export default router;
